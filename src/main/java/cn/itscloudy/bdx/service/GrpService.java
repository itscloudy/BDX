package cn.itscloudy.bdx.service;

import cn.itscloudy.bdx.entity.Grp;

import java.util.List;

public interface GrpService {
    public abstract List<Grp> findAllGrps();
}
